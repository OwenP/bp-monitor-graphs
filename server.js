
const express = require('express');
const usingExports = require('./BPMonitor.js');
const app = express();

// mounts path of middleware funtions
app.use(express.static('public'));

//Binds and listens for connections on the specified host and port
let server = app.listen(8081, (callback) => {
    var port = server.address().port;
    console.log("Server started at http://localhost:%s", port);
});

app.get('/results', (req, res) => {
    let a  = usingExports.getReadings().then( (success) =>{
        res.send(success);
        })
    });