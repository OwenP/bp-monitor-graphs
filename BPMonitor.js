const csvReader = require('./CSVReader.js');

const BP_READINGS_FILE = 'BPReadings.csv';

exports.getReadings =  async function(){
    let readings;
    try {
        readings = await csvReader.loadCSV(BP_READINGS_FILE);
        console.log('i waited 2');
    } catch(error) {
        throw error(error);
    }

    return readings;
}
