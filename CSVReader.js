const fs = require('fs');
const csvToJson = require('csvtoJson');

  exports.loadCSV = async (fileName) => {
    let readings;
    try {
        readings = await csvToJson().fromFile(fileName);

    } catch(err) {
        throw Error(`No Input file found with ${fileName}`);
    }

    return readings;
}

